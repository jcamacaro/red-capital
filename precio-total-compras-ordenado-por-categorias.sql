SELECT
compras.id,
compras.producto,
categorias.nombre AS categoria,
SUM(detalle_compras.precio) AS precio_total
FROM 
compras 
INNER JOIN detalle_compras ON compras.id = detalle_compras.compra_id
INNER JOIN categorias ON detalle_compras.categoria_id = categorias.id

GROUP BY compras.id
ORDER BY categorias.nombre ASC


