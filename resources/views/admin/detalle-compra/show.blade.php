@extends('layouts.principal')

@section('content')
	<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Detalle de Compra
        <small>Gestión</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Detalle de Compra</a></li>
        <li><a href="#">Inicio</a></li>
        <li><a href="#">Ver</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <a href="{{ url('admin/detalle-compra') }}" class="btn btn-box-tool" data-toggle="tooltip" title="Atras"><i class="fa fa-arrow-left"></i></a>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            
          </div>
        </div>
        <div class="box-body">
    
          <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="descripcion">Producto:</label>
                        <p class="form-control-static"> {{ $compra->producto }} </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="cliente_id">Cantidad:</label>
                        <p class="form-control-static"> {{ $compra->cantidad }} </p>
                    </div>
                </div>
            </div>

            <div class="box-header">
                <h3>Detalles</h3>
            </div>

             <table id="detallecompra" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Categoria</th>
                    <th>Nombre</th>
                    <th>Precio</th>
                </tr>
                </thead>
                <tbody>
                    <?php $total = 0;?>
                    @foreach($compra->detalles as $detalle)
                        <tr>
                            <td>{{$detalle->id}}</td>
                            <td>{{$detalle->categoria}}</td>
                            <td>{{$detalle->nombre}}</td>
                            <td>$ {{$detalle->precio}}</td>
                        </tr> 
                        <?php $total += $detalle->precio;?>
                    @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Categoria</th>
                    <th>Nombre</th>
                    <th>Precio</th>
                </tr>
                </tfoot>
            </table>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group text-left">
                        <p> Total: <strong> $ {{$total}} </strong> </p>
                    </div>
                </div>
            </div>


            

            
        </div>

          




        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection

@include('plugins.datatable')

@push('css')

@endpush


@push('js')

    <script type="text/javascript">
        $('document').ready(function($) {
            $("#detallecompra").DataTable({
                "language": {
                    "paginate": {
                        "previous":   "Anterior",
                        "next":       "Siguiente"
                    },
                    "search": "Buscar",
                    "lengthMenu": "Mostrar _MENU_ registros por pagina",
                    "zeroRecords": "Informacion no encontrada - Lo sentimos",
                    "info": "Muestra de pagina _PAGE_ de _PAGES_",
                    "infoEmpty": "Registros no disponibles",
                    "infoFiltered": "(filtered from _MAX_ total records)"
                },
                "aaSorting": [[ 1, "asc" ]],
            });
        });
    </script>
 
@endpush