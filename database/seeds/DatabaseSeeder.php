<?php

use App\Compra;
use App\DetalleCompra;
use App\Categoria;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	DB::statement('SET FOREIGN_KEY_CHECKS=0;');

    	DB::table('role_user')->truncate();
        DB::table('permission_role')->truncate();
        \App\Role::truncate();
        \App\User::truncate();
        \App\Permission::truncate(); 
        Compra::truncate();
        DetalleCompra::truncate();
        Categoria::truncate();
        $this->call(PermissionsSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);


        $cantidadCompras = 500;
        $cantidadCategoria = 500;
        $cantidadDetalleCompra = 500;
        // Llamando los factories
        factory( Compra::class, $cantidadCompras)->create() ;
        factory( Categoria::class, $cantidadCategoria)->create() ;
        factory( DetalleCompra::class, $cantidadCategoria)->create() ;
        

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
