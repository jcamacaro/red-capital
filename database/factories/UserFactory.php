<?php

use App\Compra;
use App\DetalleCompra;
use App\Categoria;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

/*$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});*/


$factory->define(Compra::class, function (Faker $faker) {
    return [
        'cantidad' => $faker->numberBetween(1, 10),
		'producto' =>  $faker->word
    ];
});

$factory->define(Categoria::class, function (Faker $faker) {
    return [
        'nombre' => $faker->word
    ];
});

$factory->define(DetalleCompra::class, function (Faker $faker) {    
    return [
    	'nombre' =>  $faker->name,
		'precio' =>  $faker->randomFloat(2),
		'compra_id' => Compra::all()->random()->id,
		'categoria_id' => Categoria::all()->random()->id
    ];
});






