<?php

namespace App\Http\Controllers\admin;

use App\Compra;
use App\Categoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DetalleCompra extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $compras = Compra::has('detalles')->get();

            foreach ($compras as $key => $compra) {
                
                foreach ($compra->detalles as $key => $detalle) {
                    
                    $categoria = Categoria::findOrFail($detalle->categoria_id);
                    $detalle->categoria = $categoria->nombre;
                }
            }

            return response()->json( $compras );

        }else{
            return view('admin.detalle-compra.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $compra = Compra::has('detalles')->findOrFail($id);

        foreach ($compra->detalles as $key => $detalle) {
                
            $categoria = Categoria::findOrFail($detalle->categoria_id);
            $detalle->categoria = $categoria->nombre;
        }

        return view('admin.detalle-compra.show', compact(['compra']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
