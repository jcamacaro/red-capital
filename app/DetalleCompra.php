<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleCompra extends Model
{

	protected $fillable = [
        'compra_id',
		'nombre',
		'categoria_id',
    ];

    public function compra()
    {
    	return $this->belongsTo( Compra::class );
    }

    public function categoria()
    {
    	return $this->belongsTo( Categoria::class );
    }

    public function categoriaFunction()
    {
    	$categoria = Categoria::findOrFail($this->categoria_id);
        return $this->categoria_id = $categoria;
    }
}
